/**
 * Bài tập 1
 * Input: nhập điểm chuẩn, điểm từng môn và điểm ưu tiên
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến diemChuan, diemKhuVuc, diemDoiTuong, diemMon1, diemMon2, diemMon3 theo giá trị của HTML element
 *    + Khai báo và gán biến diemTong = 0
 *    + Gán biến diemTong = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong
 *    + Dùng cấu trúc if-else:
 *        Nếu có ít nhất điểm một môn nhỏ hơn 0 thì in dòng chữ "Bạn đã rớt do có điểm thành phần nhỏ hơn hoặc bằng 0"
 *        Nếu không thì:
 *           Nếu diemTong lớn hơn hoặc bằng diemChuan thì in dòng chữ "Bạn đã đậu"  và tổng điểm
 *           Nếu không thì in dòng chữ "Bạn đã rớt"  và tổng điểm
 *
 * Output: kết quả đậu hay rớt và tổng điểm
 */
 
function baiTap1() {
  var diemChuan = document.getElementById("diem-chuan").value * 1;
  var diemKhuVuc = document.getElementById("khu-vuc").value * 1;
  var diemDoiTuong = document.getElementById("doi-tuong").value * 1;
  var diemMon1 = document.getElementById("diem-mon-1").value * 1;
  var diemMon2 = document.getElementById("diem-mon-2").value * 1;
  var diemMon3 = document.getElementById("diem-mon-3").value * 1;
  var diemTong = 0;
  
  diemTong = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;

  if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
    document.getElementById("result-b1").innerText = `Bạn đã rớt do có điểm thành phần nhỏ hơn hoặc bằng 0`;
  }
  else {
    if (diemTong >= diemChuan) {
      document.getElementById("result-b1").innerText = `Bạn đã đậu. Tổng điểm: ${diemTong}`;
    }
    else {
      document.getElementById("result-b1").innerText = `Bạn đã rớt. Tổng điểm: ${diemTong}`;
    }
  }
}

/**
 * Bài tập 2
 * Input: nhập tên và số kw
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến user, kw theo giá trị của HTML element
 *    + Khai báo và gán biến gia50KwDau = 500
 *    + Khai báo và gán biến gia50KwKe = 650
 *    + Khai báo và gán biến gia100KmKe = 850
 *    + Khai báo và gán biến gia150KmKe = 1100
 *    + Khai báo và gán biến giaConLai = 1300
 *    + Khai báo biến tongTien
 *    + Dùng cấu trúc if-else:
 *      Nếu kw <= 0 thì in dong chữ "Số kw không hợp lệ"
 *      TH còn lại:
 *        Nếu kw <= 50 thì tongTien = kw * gia50KwDau
 *        Nếu kw <= 100 thì tongTien = 50 * gia50KwDau + (kw -50) * gia50KwKe
 *        Nếu kw <= 200 thì tongTien = 50 *  gia50KwDau + 50 * gia50KwKe + (kw - 100) * gia100KmKe
 *        Nếu kw <= 350 thì tongTien = 50 *  gia50KwDau + 50 * gia50KwKe + 100 * gia100KmKe + (kw -200) * gia150KmKe
 *        TH còn lại: tongTien = 50 *  gia50KwDau + 50 * gia50KwKe + 100 * gia100KmKe + 150 * gia150KmKe + (kw - 350) * giaConLai
 *    + In kết quả ra giao diện web
 *
 * Output: tổng tiền điện theo số km đã nhập
 */

function baiTap2() {
  var user = document.getElementById("user").value;
  var kw = document.getElementById("kw").value * 1;
  var gia50KwDau = 500;
  var gia50KwKe = 650;
  var gia100KmKe = 850;
  var gia150KmKe = 1100;
  var giaConLai = 1300;
  var tongTien;

  if (kw <= 0) {
    document.getElementById("result-b2").innerText = `Số kw không hợp lệ`;
  }
  else {
    if (kw <= 50) {
      tongTien = kw * gia50KwDau;
    }
    else if (kw <= 100) {
      tongTien = 50 * gia50KwDau + (kw -50) * gia50KwKe;
    }
    else if (kw <= 200) {
      tongTien = 50 *  gia50KwDau + 50 * gia50KwKe + (kw - 100) * gia100KmKe;
    }
    else if (kw <= 350) {
      tongTien = 50 *  gia50KwDau + 50 * gia50KwKe + 100 * gia100KmKe + (kw -200) * gia150KmKe;
    }
    else {
      tongTien = 50 *  gia50KwDau + 50 * gia50KwKe + 100 * gia100KmKe + 150 * gia150KmKe + (kw - 350) * giaConLai;
    }
    document.getElementById("result-b2").innerText = `Họ tên: ${user}; Tổng tiền: ${tongTien}d`;
  }
}
